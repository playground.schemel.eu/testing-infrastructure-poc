# Start Selenoid server
Write-Host "Starting Selenoid ..."
./bin/selenoid/cm selenoid start --vnc --force -j ./conf/browsers.json

# Start Selenoid-UI server
Write-Host "Starting Selenoid-UI ..."
./bin/selenoid/cm selenoid-ui start

# Check for old report data
$reportPath = "${PWD}/tests/output"
if (Test-Path $reportPath) {
    Write-Host "Deleting old report data ..."
    rm -r -fo $reportPath
}

# Run all tests ins standard browser
Write-Host "Running all tests in standard browser ..."
docker run --rm --net=host -v ${PWD}/tests:/tests gehalt_de/codeceptjs

# Start allure server and geenrate report data
Write-Host "Starting allure server ..."
docker run -it --rm -p 1234:1234 -v ${PWD}/tests:/tests airhelp/allure allure serve -h 0.0.0.0 -p 1234 /tests/output
Write-Host "You can check the test results at http://localhost:1234"