#!/bin/bash

# Check if docker is ready
echo "Checking if docker is ready ..."

while ! docker ps 2>&1>/dev/null; do
    echo "Docker is not running. Please start it first."
    read -p "Press enter after docker is running"
done

# Make sure selenoid config directory is available
echo "Making sure selenoid config directory is available ..."

selenoidConfigPath="~/.aerokube/selenoid"

if [[ ! -d "$selenoidConfigPath" ]];
then
    echo "Creating selenoid configuratiopn directory ..."
    mkdir -p $selenoidConfigPath
fi

# Install Selenoid
echo "Downloading Selenoid ..."
./bin/selenoid/cm selenoid download

# Install Selenoid ui
echo "Downloading Selenoid UI ..."
./bin/selenoid/cm selenoid-ui download

# Build codeceptjs image
echo "Building custom codeceptjs docker image ..."
docker build -t gehalt_de/codeceptjs .

# Install dependencies via conceptjs container
echo "Install dependencies via conceptjs container ..."
docker run -it --rm -v ${PWD}/tests:/tests gehalt_de/codeceptjs npm install --save-dev --loglevel=warn

# Install allure
echo "Pulling allure container image ..."
docker pull airhelp/allure

# Describe next steps
echo "Installation of required components is finished. Use run-tests script to run the tests in default browser."
