FROM node:12-slim

RUN mkdir /tests
RUN ln -s /tests/node_modules/codeceptjs/bin/codecept.js /usr/local/bin/codeceptjs
WORKDIR /tests
CMD ["codeceptjs", "run"]
