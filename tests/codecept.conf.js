//require('/codecept/node_modules/ts-node/register')
require('ts-node/register')

exports.config = {
  tests: './tests/*_test.ts',
  output: './output',
  helpers: {
    WebDriver: {
      url: 'https://www.gehalt.de',
      browser: 'chrome',
      host: 'localhost',
      protocol: 'http', 
      port: 4444,
      path: '/wd/hub',
      windowSize: '1920x1680',
      desiredCapabilities: {
        selenoidOptions: {
          enableVNC: true,
          enableVideo: true,
        }
      }
    }
  },
  include: {
    //I: './steps_file.js'
  },
  bootstrap: null,
  mocha: {},
  name: 'test infrastructure poc',
  multiple: {
    browsers: {
      browsers: [
        {
          browser: 'chrome',
          windowSize: '1920x1680',
          desiredCapabilities: {
            selenoidOptions: {
              version: '87',
              enableVNC: true,
              enableVideo: true,
            }
          }
        },
        {
          browser: 'firefox',
          windowSize: '1920x1680',
          desiredCapabilities: {
            selenoidOptions: {
              version: '84',
              enableVNC: true,
              enableVideo: true,
            }
          }
        },
        {
          browser: 'opera',
          windowSize: '1920x1680',
          desiredCapabilities: {
            selenoidOptions: {
              version: '73',
              enableVNC: true,
              enableVideo: true,
            }
          }
        },
      ]
    },
  },
  plugins: {
    retryFailedStep: {
      enabled: true
    },
    screenshotOnFail: {
      enabled: true
    },
    allure: {
      enabled: true
    },
    stepByStepReport: {
      enabled: true,
      deleteSuccessful: false,
      screenshotsForAllureReport: true
    }
  },
}