#!/bin/bash

# Start Selenoid server
echo "Starting Selenoid ..."
./bin/selenoid/cm selenoid start --vnc --force -j ./conf/browsers.json

# Start Selenoid-UI server
echo "Starting Selenoid UI ..."
./bin/selenoid/cm selenoid-ui start

# Check for old report data
reportPath="${PWD}/tests/output"
if [[ -d "$reportPath" ]];
then
    echo "Deleting old report data ..."
    rm -rf $reportPath
fi

# Run all tests ins standard browser
echo "Running all tests in standard browser ..."
docker run --rm --net=host -v ${PWD}/tests:/tests gehalt_de/codeceptjs

# Start allure server and geenrate report data
echo "Starting allure server"
docker run -it --rm -p 1234:1234 -v ${PWD}/tests:/tests airhelp/allure allure serve -h 0.0.0.0 -p 1234 /tests/output
echo "You can check the test results via http://localhost:1234"
