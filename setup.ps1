# Check if docker is ready
Write-Host "Checking if docker is ready ..."

docker ps 2>&1>$null

while($lastexitcode -eq 1) {
    docker ps 2>&1>$null
    Write-Host "Docker is not running. Please start it first."
    Read-Host -Prompt "Press any key after docker is running"
}

# Make sure selenoid config directory is available
Write-Host "Making sure selenoid config directory is available ..."

$selenoidConfigPath = "~/.aerokube/selenoid"

if (!(Test-Path $selenoidConfigPath)) {
    Write-Host "Creating Selenoid configuratiopn directory ..."
    mkdir -p $selenoidConfigPath
}

# Install Selenoid
Write-Host "Downloading Selenoid ..."
./bin/selenoid/cm selenoid download

# Install Selenoid ui
Write-Host "Downloading Selenoid UI ..."
./bin/selenoid/cm selenoid-ui download

# Build codeceptjs image
Write-Host "Building custom codeceptjs docker image ..."
docker build -t gehalt_de/codeceptjs .

# Install dependencies via conceptjs container
Write-Host "Install dependencies via conceptjs container ..."
docker run -it --rm -v ${PWD}/tests:/tests gehalt_de/codeceptjs npm install --save-dev --loglevel=warn

# Install allure
Write-Host "Pulling allure container image ..."
docker pull airhelp/allure

# Describe next steps
Write-Host "Installation of required components is finished. Use run-tests script to run the tests in default browser."
Read-Host -Prompt "Press any key to close this window."
