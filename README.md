# testing-infrastructure-poc
Proof of concept for a dynamic testing infrastruture using CodeceptJS and Selenoid.

## Information
The following commands work on Linux, MacOS and Windows Powershell (NOT CMD).

## Installation
### Docker
Install Docker by following the instructions:
* MacOS: https://docs.docker.com/docker-for-mac/install/
* Windows: https://docs.docker.com/docker-for-windows/install/ (HyperV Installation NOT WSL2)
* Linux: https://docs.docker.com/engine/install/

### Automatic installation
Install Selenoid and Codeceptjs via script
* Windows: run `setup.bat` (via double click oder cmd)
* Linux and MacOS: `./setup.sh`

### Manual installation

#### Install Selenoid and Selenoid-UI
1. Install Selenoid by running the following command: `./bin/selenoid/cm selenoid download`
2. Install Selenoid UI by running the following command: `./bin/selenoid/cm selenoid-ui download`

#### Build CodeceptJS image
1. Build custom codeceptjs docker image: `docker build -t gehalt_de/codeceptjs .`

#### Install dependencies via conceptjs container
`docker run -it --rm -v ${PWD}/tests:/tests gehalt_de/codeceptjs npm install --save-dev --loglevel=warn`


#### (optional) Download current version of Selenoid Manager
1. (optional) Download Selenoid configuration manager to directory 'bin/selenoid'. Get the lates Version from Github. https://github.com/aerokube/cm/releases/latest
2. (optional) Rename downloaded binary to cm (on MacOS and Linux) or cm.exe (on Windows)
    1. Only for Linux and MacOS: `chmod +x cm`

## Run tests
### Automatic test run
Run Selenoid, Codeceptjs tests and allure via script
* Windows: run `run-tests.bat` (via double click oder cmd)
* Linux and MacOS: `./run-tests.sh`

### Manual test run
#### Start Selenoid and Selenoid-UI
1. Start Selenoid: `./bin/selenoid/cm selenoid start --vnc --force -j ./conf/browsers.json`
2. Start Selenoid UI: `./bin/selenoid/cm selenoid-ui start`
3. Check status in Selenoid-UI: `http://localhost:8080/#/`

#### Run tests in stadard browser only
1. Delete old reports
    * Windows(Powershell): `rm -r -fo ./tests/output`
    * Linux and MacOS: `rm -rf ./tests/output`
2. Run tests: `docker run --rm --net=host -v ${PWD}/tests:/tests gehalt_de/codeceptjs`

#### Run all tests in all configured browsers:
1. Delete old reports
    * Windows(Powershell): `rm -r -fo ./tests/output`
    * Linux and MacOS: `rm -rf ./tests/output`
2. Run tests: `docker run --rm --net=host -v ${PWD}/tests:/tests gehalt_de/codeceptjs run-multiple browsers`

#### Start allure server:
1. Start allure server: `docker run -it --rm -p 1234:1234 -v ${PWD}/tests:/tests airhelp/allure allure serve -h 0.0.0.0 -p 1234 /tests/output`
2. Check test status in allure: `http://localhost:1234`

## Shutdown Selenoid
1. Stop Selenoid: `./bin/selenoid/cm selenoid stop`
2. Stop Selenoid UI: `./bin/selenoid/cm selenoid-ui stop`

## Development
If you add new modules (e.g. pages) you have to regenerate steps.d.ts by runnning the following command.
`docker run -it -v ${PWD}/tests:/tests gehalt_de/codeceptjs npx codeceptjs def .`
